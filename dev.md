TotalPoll
================

Welcome,

You'll find in this document how TotalPoll works and how to extend it.



What TotalPoll use
-----------------------------
TotalPoll uses some built-in features in WordPress like post-type, shortcode and widget.

### Post-type

All TotalPoll polls are contained within "poll" post-type.

### Shortcode

TotalPoll make use of Shortcode API to provide "[totalpoll id="ID"]" to wide use.

### Widget

TotalPoll use also Widget API to provide a simple widget called "TP_Widget".


Reference
================

Constants
-----------------------------

#### `DS`
Directory separator.

#### `TP_TD`
Text domain.

#### `TP_PATH`
Base directory path.

#### `TP_URL`
Base directory URL.

#### `TP_TEMPLATES_PATH`
Templates directory path.

#### `TP_TEMPLATES_URL`
Templates directory URL.

#### `TP_ROOT_FILE`
Root file.

#### `TP_ADDONS_PATH`
Addons directory path.

#### `TP_ADDONS_URL`
Addons directory URL.

#### `TP_ASSETS_PATH`
Assets directory path.

#### `TP_ASSETS_URL`
Assets directory URL.

#### `TP_JS_ASSETS`
JS assets directory URL.

#### `TP_CSS_ASSETS`
CSS assets directory URL.

#### `TP_IMAGES_ASSETS`
Images assets directory URL.

#### `TP_VERSION`
Current version.

#### `TP_DIRNAME`
Directory name.

#### `TP_AJAX`
Defined when ajax call present.

Filters
-----------------------------
### tp_post_type_args


**Related filters:**

- [`tp_post_type_args`](#reference-filters-tp-post-type-args)

``` php
$args = array(
    'labels' => $labels, // Array of labels
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'poll' ),
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_position' => 2,
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title' )
);
```